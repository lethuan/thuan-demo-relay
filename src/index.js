import React from 'react'
import { render } from 'react-dom'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import viVn from 'antd/lib/locale-provider/vi_VN'
import { LocaleProvider } from 'antd'
import 'antd/dist/antd.less'
import './assets/styles/app.less'

import { LayoutView } from './screens/layout'
import { UserView } from './screens/user'
import { UserCreateView } from './screens/user/create'
import { HomeView } from './screens/home'

import { RoleView } from './screens/role'
import { RoleCreateView } from './screens/role/create'

render(
  <LocaleProvider locale={viVn}>
    <Router>
      <LayoutView>
        <Switch>
          <Route exact path="/" component={HomeView} />
          <Route exact path="/user" component={UserView} />
          <Route exact path="/user/create" component={UserCreateView} />
          <Route exact path="/user/:id/edit" component={UserCreateView} />

          <Route exact path="/roles" component={RoleView} />
          <Route exact path="/role/create" component={RoleCreateView} />
          <Route exact path="/role/:id/edit" component={RoleCreateView} />

        </Switch>
      </LayoutView>
    </Router>
  </LocaleProvider>
  , document.getElementById('root')
)
