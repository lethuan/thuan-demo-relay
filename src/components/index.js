import RcBreadcrumb from './breadcrumb'
import RcBreadcrumbs from './breadcrumbs'

export {
  RcBreadcrumb,
  RcBreadcrumbs
}
