import React from 'react'
import PropTypes from 'prop-types'
import { Breadcrumb, Icon } from 'antd'
import { Link } from 'react-router-dom'
import './style.less'

class RcBreadcrumbs extends React.Component {
  static propTypes = {
    name: PropTypes.array
  }

  render() {
    return (
      <Breadcrumb className="rc-breadcrumb">
        <Breadcrumb.Item><Icon type="shop" />&nbsp;&nbsp;Cửa hàng</Breadcrumb.Item>
        {
          this.props.name.map((item, indexItem) => {
            return (
              <Breadcrumb.Item key={indexItem}>
                <Link to={item.path} style={!item.isLink ? { pointerEvents: 'none' } : {}}>
                  {item.name}
                </Link>
              </Breadcrumb.Item>
            )
          })
        }
      </Breadcrumb>
    )
  }
}

export default RcBreadcrumbs
