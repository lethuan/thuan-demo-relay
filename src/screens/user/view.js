import React from 'react'
import lodash from 'lodash'
import { Button, Layout, Row } from 'antd'
import { Link, withRouter } from 'react-router-dom'
import { RcBreadcrumb } from '../../components'
import { ViewList } from './list'
import { CheckPermission } from '../../utils'
import './style.less'

const CRUD = ['create', 'read', 'update', 'delete']
const roleDev = ['create', 'read']

class UserView extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      userId: '',
      permissions: [],
      isCreate: true,
      isRead: true,
      isUpdate: true,
      isDelete: true,
      modalCustomerInfoVisible: false
    }
  }

  componentWillMount() {
    this.setState({
      permissions: lodash.intersection(CRUD, roleDev)
    }, () => this.checkDecentralization())
  }

  checkCRUD = (permissions) => {
    this.setState({
      isCreate: CheckPermission(permissions, CRUD).isCreate,
      isRead: CheckPermission(permissions, CRUD).isRead,
      isUpdate: CheckPermission(permissions, CRUD).isUpdate,
      isDelete: CheckPermission(permissions, CRUD).isDelete,
    })
  }
  checkDecentralization = () => {
    if (lodash.isEmpty(this.state.permissions)) {
      return this.props.history.replace('/')
    } else {
      this.checkCRUD(this.state.permissions)
    }
  }

  render() {
    return (
      <Layout className="container">
        <Row type="flex" justify="space-between">
          <RcBreadcrumb name="Quản lý thành viên" />
          <Button type="primary" className="customer-button-create" size={'large'} disabled={this.state.isCreate}>
            <Link to={'/user/create'}>
              Tạo thành viên
            </Link>
          </Button>
        </Row>
        <Layout className="page-content">
          <Row gutter={16}>
            <div className="section-title">
              {
                !this.state.isRead ? <h4>Danh sách thành viên</h4> : <h4>Bạn không có quyền xem danh sách thành viên.</h4>
              }
              {
                !this.state.isRead &&
                <ViewList disabledUpdate={this.state.isUpdate} />
              }

            </div>
          </Row>
        </Layout>
      </Layout>
    )
  }
}

export default withRouter(UserView)
