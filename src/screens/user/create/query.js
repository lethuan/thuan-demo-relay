import React from 'react'
import { graphql, QueryRenderer } from 'react-relay'
import { ENV } from '../../../configs'
import FormCreate from './form'

const queries = graphql`
  query queryShowUserQuery($userID: ID!){
    User{
      show(_id: $userID){
        name
        email
        role
      }
    }
  }
`

class CreateUserQuery extends React.Component {
  render() {
    const { id } = this.props
    if (id) {
      return (
        <QueryRenderer
          query={queries}
          environment={ENV}
          variables={{
            userID: id,
          }}
          render={({ error, props }) => {
            if (error) {
              return <div>{error.message}</div>
            } else if (props) {
              return <FormCreate id={id} query={props} />
            } else {
              return <div>.....</div>
            }
          }}
        />
      )
    } else {
      return (
        <FormCreate id={id} />
      )
    }
  }
}

export default CreateUserQuery
