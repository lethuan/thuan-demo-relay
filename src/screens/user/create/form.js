import React from 'react'
import PropTypes from 'prop-types'
import { withRouter, Link } from 'react-router-dom'
import { Form, Input, Upload, Button, Icon, Avatar, Select } from 'antd'
import { CreateUserMutation, UpdateUserMutation, UploadFileMutation } from '../../../mutations'

const FormItem = Form.Item
const Option = Select.Option
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  }
}
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 6,
    },
  }
}
const phoneValidate = /^0(1\d{9}|9\d{8})$/
const roles = [{
  id: 'manager',
  text: 'Manager'
}, {
  id: 'developer',
  text: 'Developer'
}, {
  id: 'tester',
  text: 'Tester'
}]

class UserForm extends React.Component {
  static propTypes = {
    query: PropTypes.object,
    id: PropTypes.string
  }

  constructor(props) {
    super(props)
    this.state = {
      name: '',
      email: '',
      phone: '',
      role: '',
      password: '',
      avatar: 'https://zodyapp-dev.s3.amazonaws.com/sm_766556415877_1513221612568.png',
      file: ''
    }
  }

  componentWillMount() {
    this.checkIdSetState()
  }

  onChangeName = (name) => {
    this.setState({
      name
    })
  }
  onChangeEmail = (email) => {
    this.setState({
      email
    })
  }
  onChanePassword = (password) => {
    this.setState({
      password
    })
  }
  onChangePhone = (phone) => {
    this.setState({
      phone
    })
  }
  onChangeRole = (role) => {
    this.setState({
      role
    })
  }
  onCreateUser = () => {
    const { email, name, password } = this.state
    CreateUserMutation(name, password, email, response => this.props.history.replace(`/user/${response.User.create._id}/edit`))
  }
  onUpdateUser = () => {
    const { name, phone, role } = this.state
    const { id } = this.props
    UpdateUserMutation(id, name, phone, role, () => this.props.history.replace('/user'))
  }
  uploadImage = (file) => {
    UploadFileMutation(file, response => console.log(response))
  }
  checkIdSetState = () => {
    const { id, query } = this.props
    if (id) {
      this.setState({
        name: query.User.show.name,
        email: query.User.show.email,
        role: query.User.show.role
      })
    } else {
      this.setState({
        name: '',
        email: '',
        phone: '',
        role: '',
        password: '',
      })
    }
  }

  render() {
    const { id } = this.props
    const { getFieldDecorator } = this.props.form
    return (
      <Form>
        <FormItem {...formItemLayout} label="Tên">
          <Input value={this.state.name} onChange={e => this.onChangeName(e.target.value)} />
        </FormItem>
        {
          !id && <FormItem {...formItemLayout} label="Email">
            {getFieldDecorator('email', {
              rules: [{
                type: 'email', message: 'The input is not valid E-mail!',
              }, {
                required: true, message: 'Please input your E-mail!',
              }],
              initialValue: this.state.email
            })(
              <Input onChange={e => this.onChangeEmail(e.target.value)} />
            )}
          </FormItem>
        }
        {
          !id && <FormItem {...formItemLayout} label="Mật khẩu">
            <Input value={this.state.password} onChange={e => this.onChanePassword(e.target.value)} />
          </FormItem>
        }
        {
          id && <FormItem {...formItemLayout} label="Role">
            <Select value={this.state.role} onChange={this.onChangeRole}>
              {
                roles.map((item, indexItem) => {
                  return (
                    <Option key={indexItem} value={item.id}>{item.text}</Option>
                  )
                })
              }
            </Select>
          </FormItem>
        }
        {
          id && < FormItem {...formItemLayout} label="Điện thoại">
            {
              getFieldDecorator('phone', {
                rules: [{
                  pattern: phoneValidate, message: 'Số điện thoại không đúng'
                }],
                initialValue: this.state.phone
              })(
                <Input onChange={e => this.onChangePhone(e.target.value)} />
              )
            }
          </FormItem>
        }
        <FormItem {...formItemLayout} label="Ảnh">
          <Upload
            accept="image/*"
            beforeUpload={(file) => {
              this.uploadImage(file)
              return false
            }}
            showUploadList={false}
            shape="square"
          >
            <Button>
              <Icon type="upload" /> Tải ảnh
            </Button>
          </Upload>
        </FormItem>
        <FormItem {...tailFormItemLayout}>
          <Avatar className="customer-avatar" src={this.state.avatar} />
        </FormItem>
        {
          id ? <FormItem {...tailFormItemLayout}>
            <Button type="primary" onClick={this.onUpdateUser}>Cập nhập</Button>
            <Link to="/user">
              <Button style={{ marginLeft: 10 }}>Quay lại</Button>
            </Link>
          </FormItem> : <FormItem {...tailFormItemLayout}>
            <Button type="primary" onClick={this.onCreateUser}>Tạo thành viên</Button>
            <Link to="/user">
              <Button style={{ marginLeft: 10 }}>Quay lại</Button>
            </Link>
          </FormItem>
        }
      </Form>
    )
  }
}

const FormCreate = Form.create()(UserForm)
export default withRouter(FormCreate)

