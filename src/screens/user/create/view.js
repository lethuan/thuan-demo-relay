import React from 'react'
import { Layout, Row, Col } from 'antd'
import { RcBreadcrumbs } from '../../../components'
import CreateUserView from './query'
import './style.less'

class CreateUser extends React.Component {
  render() {
    return (
      <Layout className="container">
        {
          this.props.match.params.id ?
            <RcBreadcrumbs
              name={[{ isLink: true, name: 'Thành viên', path: '/user' }, {
                isLink: false,
                name: 'Chỉnh sửa',
                path: ''
              }]}
            /> :
            <RcBreadcrumbs
              name={[{ isLink: true, name: 'Thành viên', path: '/user' }, { isLink: false, name: 'Tạo', path: '' }]}
            />
        }
        <Layout className="page-content">
          <Row>
            <div className="customer-box">
              <Col md={12} lg={12} sm={24} xs={24}>
                <CreateUserView id={this.props.match.params.id} onChangeName={this.onChangeName} />
              </Col>
            </div>
          </Row>
        </Layout>
      </Layout>
    )
  }

}

export default CreateUser
