import React from 'react'
import PropTypes from 'prop-types'
import { Modal, Row, Col, Avatar } from 'antd'
import './style.less'

class UserInfoView extends React.Component {
  static propTypes = {
    visible: PropTypes.bool,
    onClose: PropTypes.func
  }

  constructor(props) {
    super(props)
    this.state = {
      avatar: 'https://zodyapp-dev.s3.amazonaws.com/sm_766556415877_1513221612568.png'
    }
  }

  toggle = () => {
    this.props.onClose()
  }

  render() {
    const { query } = this.props
    return (
      <Modal
        title="THÔNG TIN THÀNH VIÊN"
        style={{ top: 40 }}
        width="40%"
        visible={this.props.visible}
        onOk={this.toggle}
        onCancel={this.toggle}
        afterClose={this.toggle}
        footer={null}
        className="modal-customer-data"
      >
        <Row>
          <Col xs={24} sm={24} md={24} lg={24} xl={24}>
            <Row>
              <Col md={24} lg={12} xl={8} xs={24} sm={24}>
                <Avatar src={this.state.avatar} className="profile-avatar" shape="square" />
              </Col>
              <Col md={24} lg={12} xl={12} xs={24} sm={24}>
                <table className="profile-table">
                  <tbody>
                    <tr>
                      <td>Tên</td>
                      <td className="table-data">{query.User.show.name}</td>
                    </tr>
                    <tr>
                      <td>Email</td>
                      <td className="table-data">{query.User.show.email}</td>
                    </tr>
                    <tr>
                      <td>Role</td>
                      <td className="table-data">{query.User.show.role}</td>
                    </tr>
                  </tbody>
                </table>
              </Col>
            </Row>
          </Col>
        </Row>
      </Modal>
    )
  }
}

export default UserInfoView
