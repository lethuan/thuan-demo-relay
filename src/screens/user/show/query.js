import React from 'react'
import PropTypes from 'prop-types'
import { graphql, QueryRenderer } from 'react-relay'
import { ENV } from '../../../configs'
import UserInfoView from './view'

const queries = graphql`
  query queryUserQuery($userID: ID!){
    User{
      show(_id: $userID){
        name
        email
        role
      }
    }
  }
`

class QueryUser extends React.Component {
  static propTypes = {
    userId: PropTypes.string,
    onClose: PropTypes.func,
    visible: PropTypes.bool
  }

  render() {
    const { userId, onClose, visible } = this.props
    return (
      <QueryRenderer
        query={queries}
        environment={ENV}
        variables={{
          userID: userId,
        }}
        render={({ error, props }) => {
          if (error) {
            return <div>{error.message}</div>
          } else if (props) {
            return <UserInfoView onClose={onClose} visible={visible} query={props} />
          } else {
            return <div>.....</div>
          }
        }}
      />
    )
  }
}

export default QueryUser
