import React from 'react'
import { graphql, QueryRenderer } from 'react-relay'
import { ENV } from '../../../configs'
import ViewList from './view'
import { ViewInfoUser } from '../show'

const queries = graphql`
  query queryListQuery{
      User{
        all(query: {page: 0}){
          name
          _id
          email
          role
        }
      }
  }
`

class QueryList extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      userId: '',
      modalCustomerInfoVisible: false
    }
  }

  onViewUser = (userId) => {
    this.setState({
      userId,
      modalCustomerInfoVisible: true
    })
  }

  // Close customer modal
  onCloseModalCustomerInfo = () => {
    this.setState({
      userId: '',
      modalCustomerInfoVisible: false
    })
  }

  render() {
    const { disabledUpdate } = this.props
    return (
      <div>
        <QueryRenderer
          query={queries}
          environment={ENV}
          render={({ error, props }) => {
            if (error) {
              return <div>{error.message}</div>
            } else if (props) {
              return (
                <ViewList
                  total={10}
                  pageSize={10}
                  current={0}
                  disabledUpdate={disabledUpdate}
                  data={props.User.all}
                  onViewUser={this.onViewUser}
                />
              )
            } else {
              return <div>Loading...</div>
            }
          }}
        />
        {
          this.state.modalCustomerInfoVisible && <ViewInfoUser
            visible={this.state.modalCustomerInfoVisible}
            userId={this.state.userId}
            onClose={this.onCloseModalCustomerInfo}
          />
        }
      </div>
    )
  }
}

export default QueryList
