/**
 * @flow
 * @relayHash 9c7678cc41cf30d71246b0ea5dd15791
 */

/* eslint-disable */

'use strict';

/*::
import type {ConcreteBatch} from 'relay-runtime';
export type queryListQueryResponse = {|
  +User: ?{|
    +all: $ReadOnlyArray<{|
      +name: string;
      +_id: string;
      +email: ?string;
      +role: "manager" | "developer" | "tester";
    |}>;
  |};
|};
*/


/*
query queryListQuery {
  User {
    all(query: {page: 0}) {
      name
      _id
      email
      role
    }
  }
}
*/

const batch /*: ConcreteBatch*/ = {
  "fragment": {
    "argumentDefinitions": [],
    "kind": "Fragment",
    "metadata": null,
    "name": "queryListQuery",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": null,
        "concreteType": "UserQueries",
        "name": "User",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "args": [
              {
                "kind": "Literal",
                "name": "query",
                "value": {
                  "page": 0
                },
                "type": "ALL_USERS"
              }
            ],
            "concreteType": "User",
            "name": "all",
            "plural": true,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "name",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "_id",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "email",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "role",
                "storageKey": null
              }
            ],
            "storageKey": "all{\"query\":{\"page\":0}}"
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query"
  },
  "id": null,
  "kind": "Batch",
  "metadata": {},
  "name": "queryListQuery",
  "query": {
    "argumentDefinitions": [],
    "kind": "Root",
    "name": "queryListQuery",
    "operation": "query",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": null,
        "concreteType": "UserQueries",
        "name": "User",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "args": [
              {
                "kind": "Literal",
                "name": "query",
                "value": {
                  "page": 0
                },
                "type": "ALL_USERS"
              }
            ],
            "concreteType": "User",
            "name": "all",
            "plural": true,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "name",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "_id",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "email",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "role",
                "storageKey": null
              }
            ],
            "storageKey": "all{\"query\":{\"page\":0}}"
          }
        ],
        "storageKey": null
      }
    ]
  },
  "text": "query queryListQuery {\n  User {\n    all(query: {page: 0}) {\n      name\n      _id\n      email\n      role\n    }\n  }\n}\n"
};

module.exports = batch;
