import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Row, Table, Icon } from 'antd'

const colums = (context) => {
  const { current, pageSize, disabledUpdate } = context.props
  return [{
    title: 'STT',
    dataIndex: '',
    className: 'hidden-break-small',
    render: (value, row, index) => {
      return (<span>{(current * pageSize) + (index + 1)}</span>)
    }
  }, {
    title: 'Thành viên',
    dataIndex: 'name',
    render: (value, row) => {
      return (
        <span className="registered-user" onClick={() => context.props.onViewUser(row._id)}>{value}</span>
      )
    }
  }, {
    title: 'Email',
    dataIndex: 'email',
    render: (value) => {
      return (
        <span>{value}</span>
      )
    }
  }, {
    title: 'Role',
    dataIndex: 'role',
    render: (value) => {
      return (
        <span>{value}</span>
      )
    }
  }, {
    title: 'Sửa',
    dataIndex: '_id',
    render: (value) => {
      return (
        <Link to={`/user/${value}/edit`} style={disabledUpdate ? { pointerEvents: 'none' } : {}}>
          <Icon type="edit" />
        </Link>
      )
    }
  }]
}

class ListUserView extends React.Component {
  static propTypes = {
    pageSize: PropTypes.number,
    total: PropTypes.number,
    current: PropTypes.number,
    data: PropTypes.array,
    onChange: PropTypes.func,
  }

  render() {
    const { pageSize, total, current, onChange, data } = this.props
    return (
      <Row className="background-white">
        <Table
          className="app-table"
          defaultCurrent={0}
          columns={colums(this)}
          dataSource={data}
          rowKey="_id"
          pagination={{ pageSize, total, current: current + 1 }}
          onChange={onChange}
        />
      </Row>
    )
  }
}

export default ListUserView
