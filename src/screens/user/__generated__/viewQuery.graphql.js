/**
 * @flow
 * @relayHash fb4d21ade85bd4fa11ff20e19c572fe9
 */

/* eslint-disable */

'use strict';

/*::
import type {ConcreteBatch} from 'relay-runtime';
export type viewQueryResponse = {|
  +User: ?{|
    +all: $ReadOnlyArray<{|
      +name: string;
      +_id: string;
      +email: ?string;
    |}>;
  |};
|};
*/


/*
query viewQuery {
  User {
    all(query: {page: 0}) {
      name
      _id
      email
    }
  }
}
*/

const batch /*: ConcreteBatch*/ = {
  "fragment": {
    "argumentDefinitions": [],
    "kind": "Fragment",
    "metadata": null,
    "name": "viewQuery",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": null,
        "concreteType": "UserQueries",
        "name": "User",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "args": [
              {
                "kind": "Literal",
                "name": "query",
                "value": {
                  "page": 0
                },
                "type": "ALL_USERS"
              }
            ],
            "concreteType": "User",
            "name": "all",
            "plural": true,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "name",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "_id",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "email",
                "storageKey": null
              }
            ],
            "storageKey": "all{\"query\":{\"page\":0}}"
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query"
  },
  "id": null,
  "kind": "Batch",
  "metadata": {},
  "name": "viewQuery",
  "query": {
    "argumentDefinitions": [],
    "kind": "Root",
    "name": "viewQuery",
    "operation": "query",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": null,
        "concreteType": "UserQueries",
        "name": "User",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "args": [
              {
                "kind": "Literal",
                "name": "query",
                "value": {
                  "page": 0
                },
                "type": "ALL_USERS"
              }
            ],
            "concreteType": "User",
            "name": "all",
            "plural": true,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "name",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "_id",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "email",
                "storageKey": null
              }
            ],
            "storageKey": "all{\"query\":{\"page\":0}}"
          }
        ],
        "storageKey": null
      }
    ]
  },
  "text": "query viewQuery {\n  User {\n    all(query: {page: 0}) {\n      name\n      _id\n      email\n    }\n  }\n}\n"
};

module.exports = batch;
