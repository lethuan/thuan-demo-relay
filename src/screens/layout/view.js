import React from 'react'
import { Layout } from 'antd'
import { withRouter } from 'react-router-dom'
import styles from './style.less'

import { SidebarView } from './sidebar'
import { HeaderView } from './header'

class LayoutView extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      sidebarCollapsed: false,
      isMobileScreen: false
    }
  }

  // Toggle sidebar
  toggleSidebar = () => {
    this.setState({
      sidebarCollapsed: !this.state.sidebarCollapsed
    })
  }

  // On logout
  logout = () => {
    // // Clear socket
    // this.props.dispatch({
    //   type: 'chat/logout'
    // })
    //
    // // Logout
    // this.props.dispatch({
    //   type: 'app/logout'
    // })
  }

  render() {
    const { children, location } = this.props
    const user = {
      name: 'Phat',
      address: '184 DBP',
      avatar: 'https://zodyapp-dev.s3.amazonaws.com/sm_766556415877_1513221612568.png'
    }
    return (
      <Layout className={styles.appLayout}>
        {
          user ?
            <SidebarView
              isCollapsed={this.state.sidebarCollapsed}
              location={location}
            />
            : null
        }
        <Layout style={{ minHeight: '100vh' }}>
          {
            user ?
              <HeaderView
                sidebarCollapsed={this.state.sidebarCollapsed}
                onToggleSidebar={this.toggleSidebar}
                user={user}
                logout={this.logout}
              />
              : null
          }
          <Layout>
            {children}
          </Layout>
        </Layout>
      </Layout>
    )
  }
}

export default withRouter(LayoutView)
