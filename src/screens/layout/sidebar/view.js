import React from 'react'
import PropTypes from 'prop-types'
import { Layout } from 'antd'
import { AppConst, ImageConst } from '../../../configs'
import './style.less'

// View
import { MenuView } from '../menu'

const { Sider } = Layout
let isLastMobileScreen = false

class SidebarView extends React.Component {
  static propTypes = {
    location: PropTypes.object
  }

  constructor(props) {
    super(props)

    this.state = {
      sidebarCollapsed: true,
      isMobileScreen: false
    }
  }

  // Did mount
  componentDidMount() {
    isLastMobileScreen = this.isMobileScreen()
    this.handleResize()
    window.addEventListener('resize', this.handleResize)
  }

  // Will unmount
  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize)
  }

  getScreenSize = () => {
    const screenPx = window.innerWidth
    if (screenPx <= AppConst.screens['xs-max']) { return 'xs' }
    if ((screenPx >= AppConst.screens['sm-min'])
      && (screenPx <= AppConst.screens['sm-max'])) { return 'sm' }
    if ((screenPx >= AppConst.screens['md-min'])
      && (screenPx <= AppConst.screens['md-max'])) { return 'md' }
    if ((screenPx >= AppConst.screens['lg-min'])
      && (screenPx <= AppConst.screens['lg-max'])) { return 'lg' }
    if (screenPx >= AppConst.screens['xl-min']) { return 'xl' }
  }

  isMobileScreen = () => {
    return ['xs', 'sm'].includes(this.getScreenSize())
  }

  // Handle when user resize browser
  handleResize = () => {
    this.setState({ isMobileScreen: this.isMobileScreen() })
    this.displaySidebarItem()
  }

  // Display sidebar item
  displaySidebarItem = () => {
    if (isLastMobileScreen === this.state.isMobileScreen) {
      return
    }
    isLastMobileScreen = this.state.isMobileScreen
    const items = document.getElementsByClassName('parent-menu-item')
    for (const item of items) {
      const spanTag = item.getElementsByTagName('span')
      if (spanTag && spanTag.length) {
        spanTag[0].style.display = this.state.isMobileScreen ? 'none' : 'inline-block'
      }
    }

    const submenus = document.getElementsByClassName('ant-menu-submenu')
    for (const item of submenus) {
      const parentTitle = item.getElementsByClassName('ant-menu-submenu-title')[0].getElementsByTagName('span')[0].getElementsByTagName('span')[0]
      parentTitle.style.display = this.state.isMobileScreen ? 'none' : 'inline-block'
    }
  }

  render() {
    return (
      <Sider
        className="app-sidebar"
        trigger={null}
        collapsedWidth={this.state.isMobileScreen ? 0 : 64}
        width={this.state.isMobileScreen ? 64 : 200}
        collapsible
        collapsed={this.props.isCollapsed}
      >
        <img className="app-logo" src={ImageConst.logoZody} alt="" />
        <MenuView
          location={this.props.location}
          mode={!this.state.isMobileScreen ? 'inline' : 'vertical'}
        />
      </Sider>
    )
  }
}

SidebarView.__ANT_LAYOUT_SIDER = true /* eslint no-underscore-dangle: ["error", { "allow": ["__ANT_LAYOUT_SIDER"] }] */
export default SidebarView
