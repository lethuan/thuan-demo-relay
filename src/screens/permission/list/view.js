import React, { Component } from 'react'
import { Select } from 'antd'

const Option = Select.Option

class PermissionView extends Component {
  constructor(props) {
    super(props)
    this.state = {
      permission: []
    }
  }
  handleChange = (permission) => {
    this.setState({
      permission
    })
    this.props.onChange(permission)
  }
  render() {
    const childrens = this.props.data.Permission.all
    const permisions = []
    childrens.map((children, indexItem) => {
      permisions.push(<Option key={indexItem} value={children}>{children}</Option>)
      return permisions
    })
    return (
      <div>
        <Select
          mode="multiple"
          style={{ width: '100%' }}
          placeholder="Selecte permission"
          onChange={this.handleChange}
        >
          {permisions}
        </Select>
      </div>
    )
  }
}

export default PermissionView
