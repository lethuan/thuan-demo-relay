import React from 'react'
import { graphql, QueryRenderer } from 'react-relay'
import { ENV } from '../../../configs'
import { PermissionsView } from '../../permission/list'

const queries = graphql`
  query queryCreateRoleQueryQuery{
    Permission{
      all
    }
  }
`

class CreateRoleQuery extends React.Component {
  render() {
    const { onChange } = this.props
    return (
      <QueryRenderer
        query={queries}
        environment={ENV}
        render={({ error, props }) => {
          if (error) {
            return <div>{error.message}</div>
          } else if (props) {
            return <PermissionsView data={props} onChange={onChange} />
          } else {
            return <div>Loading</div>
          }
        }}
      />
    )
  }
}

export default CreateRoleQuery
