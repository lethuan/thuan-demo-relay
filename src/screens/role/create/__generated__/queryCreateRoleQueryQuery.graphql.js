/**
 * @flow
 * @relayHash 4b303cecd484e22d8480e01fb7316772
 */

/* eslint-disable */

'use strict';

/*::
import type {ConcreteBatch} from 'relay-runtime';
export type queryCreateRoleQueryQueryResponse = {|
  +Permission: ?{|
    +all: ?$ReadOnlyArray<string>;
  |};
|};
*/


/*
query queryCreateRoleQueryQuery {
  Permission {
    all
  }
}
*/

const batch /*: ConcreteBatch*/ = {
  "fragment": {
    "argumentDefinitions": [],
    "kind": "Fragment",
    "metadata": null,
    "name": "queryCreateRoleQueryQuery",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": null,
        "concreteType": "PemissionQueries",
        "name": "Permission",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "args": null,
            "name": "all",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query"
  },
  "id": null,
  "kind": "Batch",
  "metadata": {},
  "name": "queryCreateRoleQueryQuery",
  "query": {
    "argumentDefinitions": [],
    "kind": "Root",
    "name": "queryCreateRoleQueryQuery",
    "operation": "query",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": null,
        "concreteType": "PemissionQueries",
        "name": "Permission",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "args": null,
            "name": "all",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "text": "query queryCreateRoleQueryQuery {\n  Permission {\n    all\n  }\n}\n"
};

module.exports = batch;
