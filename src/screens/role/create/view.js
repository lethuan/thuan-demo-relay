import React from 'react'
import { Form, Layout, Row, Col, Input, Button } from 'antd'
import { CreateRoleMutation } from '../../../mutations/role'
import CreateRoleQuery from './query'
import { RcBreadcrumb } from '../../../components'
import { UpdateRoleQuery } from '../update'
import './style.less'

const FormItem = Form.Item
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 6,
    },
  },
};

class RoleCreateView extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      permission: []
    }
  }

  onchangeName = (name) => {
    this.setState({
      name
    })
  }
  onchangePermission = (permission) => {
    this.setState({
      permission
    })
  }
  createRole = () => {
    const { name, permission } = this.state
    CreateRoleMutation(name, permission, () => this.props.history.replace('/roles'))
    this.setState({
      name: '',
      permission: []
    })
  }
  render() {
    const { id } = this.props.match.params
    const { history } = this.props
    if (id) {
      return (
        <Layout className="container">
          <RcBreadcrumb name="Quản Lý Role / Chỉnh sửa" />
          <Layout className="page-content">
            <Row>
              <div className="customer-box">
                <Col md={12} lg={12} sm={24} xs={24}>
                  <UpdateRoleQuery id={id} history={history} />
                </Col>
              </div>
            </Row>
          </Layout>
        </Layout>
      )
    } else {
      return (
        <Layout className="container">
          <RcBreadcrumb name="Quản Lý Role / Tạo Role" />
          <Layout className="page-content">
            <Row>
              <div className="customer-box">
                <Col md={12} lg={12} sm={24} xs={24}>
                  <Form>
                    <FormItem {...formItemLayout} label="name">
                      <Input onChange={e => this.onchangeName(e.target.value)} />
                    </FormItem>
                    <FormItem {...formItemLayout} label="Permission">
                      <CreateRoleQuery onChange={this.onchangePermission} />
                    </FormItem>
                    <FormItem {...tailFormItemLayout}>
                      <Button type="primary" onClick={this.createRole}>Tạo role</Button>
                    </FormItem>
                  </Form>
                </Col>
              </div>
            </Row>
          </Layout>
        </Layout>
      )
    }
  }

}

export default RoleCreateView
