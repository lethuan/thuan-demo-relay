import React from 'react'
import { graphql, QueryRenderer } from 'react-relay'
import { ENV } from '../../../configs'
import ViewList from './view'

const queries = graphql`
  query queryListQueryRoleQuery{
    Role{
      all(query: {page: 0}){
        _id
        name
        permissions
      }
    }
  }
`

class QueryListRole extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      roleId: ''
    }
  }

  render() {
    return (
      <QueryRenderer
        query={queries}
        environment={ENV}
        render={({ error, props }) => {
          if (error) {
            return <div>{error.message}</div>
          } else if (props) {
            return (
              <ViewList
                total={10}
                pageSize={10}
                current={0}
                data={props.Role.all}
                onViewRole={this.props.onViewRole}
                onConfirm={this.props.onConfirm}
                onCancel={this.props.onCancel}
                onChange={this.onChange}
              />
            )
          } else {
            return <div>Loading...</div>
          }
        }}
      />
    )
  }
}

export default QueryListRole
