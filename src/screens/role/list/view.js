import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Row, Table, Icon, Popconfirm } from 'antd'

const colums = (context) => {
  const { current, pageSize } = context.props
  return [{
    title: 'STT',
    dataIndex: '',
    className: 'hidden-break-small',
    render: (value, row, index) => {
      return (<span>{(current * pageSize) + (index + 1)}</span>)
    }
  }, {
    title: 'Role',
    dataIndex: 'name',
    render: (value, row) => {
      return (
        <span className="registered-user" onClick={() => context.props.onViewRole(row._id)}>{value}</span>
      )
    }
  }, {
    title: 'Permissions',
    dataIndex: 'permissions',
    render: (value) => {
      return (
        <div>
          {value}
        </div>
      )
    }
  }, {
    title: 'Sửa',
    dataIndex: '_id',
    render: (value) => {
      return (
        <Link to={`/role/${value}/edit`}>
          <Icon type="edit" />
        </Link>
      )
    }
  }, {
    title: 'Xóa',
    dataIndex: '',
    render: (value, row) => {
      return (
        <Popconfirm
          title="Are you sure delete this role?"
          onConfirm={() => context.props.onConfirm(row._id)}
          onCancel={() => context.props.onCancel(row._id)}
          okText="Yes" cancelText="No"
        >
          <Icon type="delete" />
        </Popconfirm>
      )
    }
  }]
}
class ViewListRole extends React.Component {
  static propTypes = {
    pageSize: PropTypes.number,
    total: PropTypes.number,
    current: PropTypes.number,
    data: PropTypes.array,
    onChange: PropTypes.func,
  }

  render() {
    const { pageSize, total, current, onChange, data } = this.props
    return (
      <Row className="background-white">
        <Table
          className="app-table"
          defaultCurrent={0}
          columns={colums(this)}
          dataSource={data}
          rowKey="_id"
          pagination={{ pageSize, total, current: current + 1 }}
          onChange={onChange}
        />
      </Row>
    )
  }
}

export default ViewListRole
