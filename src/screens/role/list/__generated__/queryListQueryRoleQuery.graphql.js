/**
 * @flow
 * @relayHash 474cff63e6b87969d79b76794355bbdd
 */

/* eslint-disable */

'use strict';

/*::
import type {ConcreteBatch} from 'relay-runtime';
export type queryListQueryRoleQueryResponse = {|
  +Role: ?{|
    +all: $ReadOnlyArray<{|
      +_id: string;
      +name: string;
      +permissions: $ReadOnlyArray<?string>;
    |}>;
  |};
|};
*/


/*
query queryListQueryRoleQuery {
  Role {
    all(query: {page: 0}) {
      _id
      name
      permissions
    }
  }
}
*/

const batch /*: ConcreteBatch*/ = {
  "fragment": {
    "argumentDefinitions": [],
    "kind": "Fragment",
    "metadata": null,
    "name": "queryListQueryRoleQuery",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": null,
        "concreteType": "RoleQueries",
        "name": "Role",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "args": [
              {
                "kind": "Literal",
                "name": "query",
                "value": {
                  "page": 0
                },
                "type": "ALL_ROLE"
              }
            ],
            "concreteType": "Role",
            "name": "all",
            "plural": true,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "_id",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "name",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "permissions",
                "storageKey": null
              }
            ],
            "storageKey": "all{\"query\":{\"page\":0}}"
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query"
  },
  "id": null,
  "kind": "Batch",
  "metadata": {},
  "name": "queryListQueryRoleQuery",
  "query": {
    "argumentDefinitions": [],
    "kind": "Root",
    "name": "queryListQueryRoleQuery",
    "operation": "query",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": null,
        "concreteType": "RoleQueries",
        "name": "Role",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "args": [
              {
                "kind": "Literal",
                "name": "query",
                "value": {
                  "page": 0
                },
                "type": "ALL_ROLE"
              }
            ],
            "concreteType": "Role",
            "name": "all",
            "plural": true,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "_id",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "name",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "permissions",
                "storageKey": null
              }
            ],
            "storageKey": "all{\"query\":{\"page\":0}}"
          }
        ],
        "storageKey": null
      }
    ]
  },
  "text": "query queryListQueryRoleQuery {\n  Role {\n    all(query: {page: 0}) {\n      _id\n      name\n      permissions\n    }\n  }\n}\n"
};

module.exports = batch;
