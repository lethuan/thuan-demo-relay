import React from 'react'
import { Button, Layout, Row } from 'antd'
import { Link } from 'react-router-dom'
import { RcBreadcrumb } from '../../components'
import { QueryRole } from './show'
import { QueryListRole } from './list'
import './style.less'
import { DeleteRoleMutation } from '../../mutations/role'

class RoleView extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      roleId: '',
      modalCustomerInfoVisible: false
    }
  }

  onViewRole = (roleId) => {
    this.setState({
      roleId,
      modalCustomerInfoVisible: true
    })
  }

  // Close customer modal
  onCloseModalCustomerInfo = () => {
    this.setState({
      roleId: '',
      modalCustomerInfoVisible: false
    })
  }
  onConfirm = (roleId) => {
    DeleteRoleMutation(roleId, () => {
      console.log('delete successfully !')
      // window.location.reload()
    })
  }
  onCancel = () => {
    console.log('cancel allready !')
  }

  render() {
    return (
      <Layout className="container">
        <Row type="flex" justify="space-between">
          <RcBreadcrumb name="Quản lý Role" />
          <Link to={'/role/create'}>
            <Button type="primary" className="customer-button-create" size={'large'}>Tạo Role</Button>
          </Link>
        </Row>
        <Layout className="page-content">
          <Row gutter={16}>
            <div className="section-title">
              <h4>Danh sách Role</h4>
              <QueryListRole onViewRole={this.onViewRole} onConfirm={this.onConfirm} onCancel={this.onCancel} />
            </div>
          </Row>
          {
            this.state.modalCustomerInfoVisible && <QueryRole
              visible={this.state.modalCustomerInfoVisible}
              roleId={this.state.roleId}
              onClose={this.onCloseModalCustomerInfo}
            />
          }
        </Layout>
      </Layout>
    )
  }
}

export default RoleView
