import React from 'react'
import PropTypes from 'prop-types'
import { graphql, QueryRenderer } from 'react-relay'
import { ENV } from '../../../configs'
import RoleInfoView from './view'

const queries = graphql`
  query queryRoleQuery($input: ID){
    Role{
      show(_id: $input){
        _id
        name
        permissions
      }
    }
  }
`

class QueryRole extends React.Component {
  static propTypes = {
    roleId: PropTypes.string,
    onClose: PropTypes.func,
    visible: PropTypes.bool
  }

  render() {
    const { roleId, onClose, visible } = this.props
    return (
      <QueryRenderer
        query={queries}
        environment={ENV}
        variables={{
          input: roleId
        }}
        render={({ error, props }) => {
          if (error) {
            return <div>{error.message}</div>
          } else if (props) {
            return <RoleInfoView onClose={onClose} visible={visible} query={props} />
          } else {
            return <div>.....</div>
          }
        }}
      />
    )
  }
}

export default QueryRole
