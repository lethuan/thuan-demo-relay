import React from 'react'
import PropTypes from 'prop-types'
import { Modal, Row, Col } from 'antd'
import './style.less'

class RoleInfoView extends React.Component {
  static propTypes = {
    visible: PropTypes.bool,
    onClose: PropTypes.func
  }

  constructor(props) {
    super(props)
    this.state = {
    }
  }

  toggle = () => {
    this.props.onClose()
  }

  render() {
    const { query } = this.props
    return (
      <Modal
        title="Thông Tin Role"
        style={{ top: 40 }}
        width="40%"
        visible={this.props.visible}
        onOk={this.toggle}
        onCancel={this.toggle}
        afterClose={this.toggle}
        footer={null}
        className="modal-customer-data"
      >
        <Row>
          <Col xs={24} sm={24} md={24} lg={24} xl={24}>
            <Row>
              <Col md={24} lg={24} xl={24} xs={24} sm={24}>
                <table className="profile-table">
                  <tbody>
                    <tr>
                      <td>Tên</td>
                      <td className="table-data">{query.Role.show.name}</td>
                    </tr>
                    <tr>
                      <td>Active</td>
                      <td className="table-data">{query.Role.show.active ? 'True' : 'False'}</td>
                    </tr>
                    <tr>
                      <td>Permissions</td>
                      <td className="table-data">
                        {query.Role.show.permissions.map((permission) => { return <span key={permission._id}>{permission.name} </span> })}
                      </td>
                    </tr>
                  </tbody>
                </table>
              </Col>
            </Row>
          </Col>
        </Row>
      </Modal>
    )
  }
}

export default RoleInfoView
