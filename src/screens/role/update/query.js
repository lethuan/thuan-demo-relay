import React, { Component } from 'react'
import { QueryRenderer, graphql } from 'react-relay'
import ENV from '../../../configs/createRelayEnvironment'
import UpdateRoleView from './view'

const queries = graphql`
  query queryUpdateRoleQuery($input: ID!){
    Role{
      show(_id: $input){
        _id
        name
        permissions
      }
    }
    Permission{
      all
    }
  }
`
class UpdateRoleQuery extends Component {
  render() {
    const { id, history } = this.props
    return (

      <QueryRenderer
        environment={ENV}
        query={queries}
        variables={{
          input: id
        }}
        render={({ error, props }) => {
          if (error) {
            return <div>{error.message}</div>
          } else if (props) {
            return (
              <UpdateRoleView data={props} history={history} />
            )
          } else {
            return (
              <div>Loading...</div>
            )
          }
        }}
      />
    )
  }
}
export default UpdateRoleQuery
