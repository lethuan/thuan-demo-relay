import React, { Component } from 'react'
import { Form, Input, Button, Select } from 'antd'
import { UpdateRoleMutation } from '../../../mutations/role'

const Option = Select.Option

const FormItem = Form.Item
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 6,
    },
  },
};

class UpdateRoleView extends Component {
  constructor(props) {
    super(props)
    this.state = {
      name: this.props.data.Role.show.name,
      permission: this.props.data.Role.show.permissions

    }
  }
  onchangePermission = (permission) => {
    this.setState({
      permission
    })
  }
  onchangeName = (name) => {
    this.setState({
      name
    })
  }
  updateRole = () => {
    const { name, permission } = this.state
    const roleId = this.props.data.Role.show._id
    UpdateRoleMutation(roleId, name, permission, () => this.props.history.replace('/roles'))
  }
  render() {
    const permisions = []
    const childrens = this.props.data.Permission.all
    childrens.map((children, indexItem) => {
      permisions.push(<Option key={indexItem} value={children}>{children}</Option>)
      return permisions
    })
    return (
      <Form>
        <FormItem {...formItemLayout} label="name">
          <Input onChange={e => this.onchangeName(e.target.value)} value={this.state.name} />
        </FormItem>
        <FormItem {...formItemLayout} label="Permission">
          <div>
            <Select
              mode="multiple"
              style={{ width: '100%' }}
              placeholder="Selecte permission"
              onChange={this.onchangePermission}
              value={this.state.permission}
            >
              {permisions}
            </Select>
          </div>
        </FormItem>
        <FormItem {...tailFormItemLayout}>
          <Button type="primary" onClick={this.updateRole}>Chỉnh sửa</Button>
        </FormItem>
      </Form>
    )
  }
}
export default UpdateRoleView

