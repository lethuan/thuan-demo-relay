/**
 * @flow
 * @relayHash 936c99fa72b17ba7afb3410bc0008a79
 */

/* eslint-disable */

'use strict';

/*::
import type {ConcreteBatch} from 'relay-runtime';
export type queryUpdateRoleQueryResponse = {|
  +Role: ?{|
    +show: {|
      +_id: string;
      +name: string;
      +permissions: $ReadOnlyArray<?string>;
    |};
  |};
  +Permission: ?{|
    +all: ?$ReadOnlyArray<string>;
  |};
|};
*/


/*
query queryUpdateRoleQuery(
  $input: ID!
) {
  Role {
    show(_id: $input) {
      _id
      name
      permissions
    }
  }
  Permission {
    all
  }
}
*/

const batch /*: ConcreteBatch*/ = {
  "fragment": {
    "argumentDefinitions": [
      {
        "kind": "LocalArgument",
        "name": "input",
        "type": "ID!",
        "defaultValue": null
      }
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "queryUpdateRoleQuery",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": null,
        "concreteType": "RoleQueries",
        "name": "Role",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "args": [
              {
                "kind": "Variable",
                "name": "_id",
                "variableName": "input",
                "type": "ID"
              }
            ],
            "concreteType": "Role",
            "name": "show",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "_id",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "name",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "permissions",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      },
      {
        "kind": "LinkedField",
        "alias": null,
        "args": null,
        "concreteType": "PemissionQueries",
        "name": "Permission",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "args": null,
            "name": "all",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query"
  },
  "id": null,
  "kind": "Batch",
  "metadata": {},
  "name": "queryUpdateRoleQuery",
  "query": {
    "argumentDefinitions": [
      {
        "kind": "LocalArgument",
        "name": "input",
        "type": "ID!",
        "defaultValue": null
      }
    ],
    "kind": "Root",
    "name": "queryUpdateRoleQuery",
    "operation": "query",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": null,
        "concreteType": "RoleQueries",
        "name": "Role",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "args": [
              {
                "kind": "Variable",
                "name": "_id",
                "variableName": "input",
                "type": "ID"
              }
            ],
            "concreteType": "Role",
            "name": "show",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "_id",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "name",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "permissions",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      },
      {
        "kind": "LinkedField",
        "alias": null,
        "args": null,
        "concreteType": "PemissionQueries",
        "name": "Permission",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "args": null,
            "name": "all",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "text": "query queryUpdateRoleQuery(\n  $input: ID!\n) {\n  Role {\n    show(_id: $input) {\n      _id\n      name\n      permissions\n    }\n  }\n  Permission {\n    all\n  }\n}\n"
};

module.exports = batch;
