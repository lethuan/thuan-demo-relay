import CreateUserMutation from './user/create_user_mutation'
import UpdateUserMutation from './user/update_user_mutation'
import UploadFileMutation from './upload/upload-file'

export {
  CreateUserMutation,
  UpdateUserMutation,
  UploadFileMutation
}
