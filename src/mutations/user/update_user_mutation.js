import { commitMutation, graphql } from 'react-relay'
import { ENV } from '../../configs'

const mutation = graphql`
    mutation updateUserMutation($userID: ID!,$input: UPDATE_USER){
      User{
        update(_id: $userID,body: $input){
          name
        }
      }
    }
  `
export default (ID, name, phone, role, callback) => {
  const variables = {
    userID: ID,
    input: {
      name,
      phone,
      role
    }
  }
  commitMutation(
    ENV,
    {
      mutation,
      variables,
      onCompleted: () => {
        callback()
      },
      onError: err => console.log(err)
    }
  )
}
