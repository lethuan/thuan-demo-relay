import { commitMutation, graphql } from 'react-relay'
import { ENV } from '../../configs'

const mutation = graphql`
    mutation createUserMutation($input: CREATE_USER){
        User{
          create(body: $input){
            _id
            name
          }
        }
    }
  `
export default (name, password, email, callback) => {
  const variables = {
    input: {
      name,
      password,
      email
    }
  }
  commitMutation(
    ENV,
    {
      mutation,
      variables,
      onCompleted: (response) => {
        callback(response)
      },
      onError: err => console.log(err)
    }
  )
}
