/**
 * @flow
 * @relayHash c18e69b9f5c22c9d7ce2e050a446bc62
 */

/* eslint-disable */

'use strict';

/*::
import type {ConcreteBatch} from 'relay-runtime';
export type createUserMutationVariables = {|
  input?: ?{
    name: string;
    email: string;
    password: string;
    role: "manager" | "developer" | "tester";
  };
|};
export type createUserMutationResponse = {|
  +User: ?{|
    +create: {|
      +_id: string;
      +name: string;
    |};
  |};
|};
*/


/*
mutation createUserMutation(
  $input: CREATE_USER
) {
  User {
    create(body: $input) {
      _id
      name
    }
  }
}
*/

const batch /*: ConcreteBatch*/ = {
  "fragment": {
    "argumentDefinitions": [
      {
        "kind": "LocalArgument",
        "name": "input",
        "type": "CREATE_USER",
        "defaultValue": null
      }
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "createUserMutation",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": null,
        "concreteType": "UserMutations",
        "name": "User",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "args": [
              {
                "kind": "Variable",
                "name": "body",
                "variableName": "input",
                "type": "CREATE_USER"
              }
            ],
            "concreteType": "User",
            "name": "create",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "_id",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "name",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Mutation"
  },
  "id": null,
  "kind": "Batch",
  "metadata": {},
  "name": "createUserMutation",
  "query": {
    "argumentDefinitions": [
      {
        "kind": "LocalArgument",
        "name": "input",
        "type": "CREATE_USER",
        "defaultValue": null
      }
    ],
    "kind": "Root",
    "name": "createUserMutation",
    "operation": "mutation",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": null,
        "concreteType": "UserMutations",
        "name": "User",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "args": [
              {
                "kind": "Variable",
                "name": "body",
                "variableName": "input",
                "type": "CREATE_USER"
              }
            ],
            "concreteType": "User",
            "name": "create",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "_id",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "name",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "text": "mutation createUserMutation(\n  $input: CREATE_USER\n) {\n  User {\n    create(body: $input) {\n      _id\n      name\n    }\n  }\n}\n"
};

module.exports = batch;
