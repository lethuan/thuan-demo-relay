/**
 * @flow
 * @relayHash e8d6ca3a4b39553af4267eb29e0c385b
 */

/* eslint-disable */

'use strict';

/*::
import type {ConcreteBatch} from 'relay-runtime';
export type updateUserMutationVariables = {|
  userID: string;
  input?: ?{
    name?: ?string;
    role?: ?"manager" | "developer" | "tester";
    phone?: ?string;
  };
|};
export type updateUserMutationResponse = {|
  +User: ?{|
    +update: {|
      +name: string;
    |};
  |};
|};
*/


/*
mutation updateUserMutation(
  $userID: ID!
  $input: UPDATE_USER
) {
  User {
    update(_id: $userID, body: $input) {
      name
    }
  }
}
*/

const batch /*: ConcreteBatch*/ = {
  "fragment": {
    "argumentDefinitions": [
      {
        "kind": "LocalArgument",
        "name": "userID",
        "type": "ID!",
        "defaultValue": null
      },
      {
        "kind": "LocalArgument",
        "name": "input",
        "type": "UPDATE_USER",
        "defaultValue": null
      }
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "updateUserMutation",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": null,
        "concreteType": "UserMutations",
        "name": "User",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "args": [
              {
                "kind": "Variable",
                "name": "_id",
                "variableName": "userID",
                "type": "ID!"
              },
              {
                "kind": "Variable",
                "name": "body",
                "variableName": "input",
                "type": "UPDATE_USER"
              }
            ],
            "concreteType": "User",
            "name": "update",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "name",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Mutation"
  },
  "id": null,
  "kind": "Batch",
  "metadata": {},
  "name": "updateUserMutation",
  "query": {
    "argumentDefinitions": [
      {
        "kind": "LocalArgument",
        "name": "userID",
        "type": "ID!",
        "defaultValue": null
      },
      {
        "kind": "LocalArgument",
        "name": "input",
        "type": "UPDATE_USER",
        "defaultValue": null
      }
    ],
    "kind": "Root",
    "name": "updateUserMutation",
    "operation": "mutation",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": null,
        "concreteType": "UserMutations",
        "name": "User",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "args": [
              {
                "kind": "Variable",
                "name": "_id",
                "variableName": "userID",
                "type": "ID!"
              },
              {
                "kind": "Variable",
                "name": "body",
                "variableName": "input",
                "type": "UPDATE_USER"
              }
            ],
            "concreteType": "User",
            "name": "update",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "name",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "text": "mutation updateUserMutation(\n  $userID: ID!\n  $input: UPDATE_USER\n) {\n  User {\n    update(_id: $userID, body: $input) {\n      name\n    }\n  }\n}\n"
};

module.exports = batch;
