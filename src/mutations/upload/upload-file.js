import { commitMutation, graphql } from 'react-relay'
import environment from '../../relay-environment'

const mutation = graphql`
    mutation uploadFileMutation($file: FileUpload!){
      Upload{
        upload(file: $file) {
          url
        }
      }
    }
  `
export default (file, callback) => {
  const variables = {
    file
  }
  let uploadables

  if (file) {
    uploadables = { file }
  }
  commitMutation(environment, {
    mutation,
    variables,
    uploadables,
    onCompleted: (response) => {
      callback(response)
    },
    onError: err => console.log(err)
  })
}
