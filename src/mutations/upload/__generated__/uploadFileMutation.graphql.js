/**
 * @flow
 * @relayHash 7d29964d3e8ca23bc8096e37a89f03b7
 */

/* eslint-disable */

'use strict';

/*::
import type {ConcreteBatch} from 'relay-runtime';
export type uploadFileMutationVariables = {|
  file: any;
|};
export type uploadFileMutationResponse = {|
  +Upload: ?{|
    +upload: ?{|
      +url: string;
    |};
  |};
|};
*/


/*
mutation uploadFileMutation(
  $file: FileUpload!
) {
  Upload {
    upload(file: $file) {
      url
    }
  }
}
*/

const batch /*: ConcreteBatch*/ = {
  "fragment": {
    "argumentDefinitions": [
      {
        "kind": "LocalArgument",
        "name": "file",
        "type": "FileUpload!",
        "defaultValue": null
      }
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "uploadFileMutation",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": null,
        "concreteType": "UploadMutations",
        "name": "Upload",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "args": [
              {
                "kind": "Variable",
                "name": "file",
                "variableName": "file",
                "type": "FileUpload!"
              }
            ],
            "concreteType": "Upload",
            "name": "upload",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "url",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Mutation"
  },
  "id": null,
  "kind": "Batch",
  "metadata": {},
  "name": "uploadFileMutation",
  "query": {
    "argumentDefinitions": [
      {
        "kind": "LocalArgument",
        "name": "file",
        "type": "FileUpload!",
        "defaultValue": null
      }
    ],
    "kind": "Root",
    "name": "uploadFileMutation",
    "operation": "mutation",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": null,
        "concreteType": "UploadMutations",
        "name": "Upload",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "args": [
              {
                "kind": "Variable",
                "name": "file",
                "variableName": "file",
                "type": "FileUpload!"
              }
            ],
            "concreteType": "Upload",
            "name": "upload",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "url",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "text": "mutation uploadFileMutation(\n  $file: FileUpload!\n) {\n  Upload {\n    upload(file: $file) {\n      url\n    }\n  }\n}\n"
};

module.exports = batch;
