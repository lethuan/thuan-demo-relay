import CreateRoleMutation from './create-role-mutation'
import DeleteRoleMutation from './delete-role-mutation'
import UpdateRoleMutation from './update-role-mutation'

export {
  CreateRoleMutation,
  DeleteRoleMutation,
  UpdateRoleMutation
}
