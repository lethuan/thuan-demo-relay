// 1
import {
  commitMutation,
  graphql,
} from 'react-relay'
// import { ConnectionHandler } from 'relay-runtime'
import environment from '../../configs/createRelayEnvironment'

// 2
const mutation = graphql`
mutation updateRoleMutation($inputId: ID!, $inputBody: UPDATE_ROLE){
  Role{
    update(_id: $inputId, body: $inputBody){
      _id
      name
      permissions
    }
  }
}
`

// 3
export default (roleId, name, permissions, callback) => {
  // 4
  const variables = {
    inputId: roleId,
    inputBody: {
      name,
      permissions
    }
  }

  // 5
  commitMutation(
    environment,
    {
      mutation,
      variables,
      // 6
      onCompleted: () => {
        callback()
      },
      onError: err => console.error(err),
    },
  )
}
