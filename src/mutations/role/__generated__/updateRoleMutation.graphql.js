/**
 * @flow
 * @relayHash 594457a7a6c150b7b62f5540c1226e9a
 */

/* eslint-disable */

'use strict';

/*::
import type {ConcreteBatch} from 'relay-runtime';
export type updateRoleMutationVariables = {|
  inputId: string;
  inputBody?: ?{
    name?: ?string;
    permissions?: ?$ReadOnlyArray<string>;
  };
|};
export type updateRoleMutationResponse = {|
  +Role: ?{|
    +update: ?{|
      +_id: string;
      +name: string;
      +permissions: $ReadOnlyArray<?string>;
    |};
  |};
|};
*/


/*
mutation updateRoleMutation(
  $inputId: ID!
  $inputBody: UPDATE_ROLE
) {
  Role {
    update(_id: $inputId, body: $inputBody) {
      _id
      name
      permissions
    }
  }
}
*/

const batch /*: ConcreteBatch*/ = {
  "fragment": {
    "argumentDefinitions": [
      {
        "kind": "LocalArgument",
        "name": "inputId",
        "type": "ID!",
        "defaultValue": null
      },
      {
        "kind": "LocalArgument",
        "name": "inputBody",
        "type": "UPDATE_ROLE",
        "defaultValue": null
      }
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "updateRoleMutation",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": null,
        "concreteType": "RoleMutations",
        "name": "Role",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "args": [
              {
                "kind": "Variable",
                "name": "_id",
                "variableName": "inputId",
                "type": "ID!"
              },
              {
                "kind": "Variable",
                "name": "body",
                "variableName": "inputBody",
                "type": "UPDATE_ROLE"
              }
            ],
            "concreteType": "Role",
            "name": "update",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "_id",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "name",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "permissions",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Mutation"
  },
  "id": null,
  "kind": "Batch",
  "metadata": {},
  "name": "updateRoleMutation",
  "query": {
    "argumentDefinitions": [
      {
        "kind": "LocalArgument",
        "name": "inputId",
        "type": "ID!",
        "defaultValue": null
      },
      {
        "kind": "LocalArgument",
        "name": "inputBody",
        "type": "UPDATE_ROLE",
        "defaultValue": null
      }
    ],
    "kind": "Root",
    "name": "updateRoleMutation",
    "operation": "mutation",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": null,
        "concreteType": "RoleMutations",
        "name": "Role",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "args": [
              {
                "kind": "Variable",
                "name": "_id",
                "variableName": "inputId",
                "type": "ID!"
              },
              {
                "kind": "Variable",
                "name": "body",
                "variableName": "inputBody",
                "type": "UPDATE_ROLE"
              }
            ],
            "concreteType": "Role",
            "name": "update",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "_id",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "name",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "permissions",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "text": "mutation updateRoleMutation(\n  $inputId: ID!\n  $inputBody: UPDATE_ROLE\n) {\n  Role {\n    update(_id: $inputId, body: $inputBody) {\n      _id\n      name\n      permissions\n    }\n  }\n}\n"
};

module.exports = batch;
