/**
 * @flow
 * @relayHash a1e916d7048f1ea5eb4008cf487ebe5d
 */

/* eslint-disable */

'use strict';

/*::
import type {ConcreteBatch} from 'relay-runtime';
export type deleteRoleMutationVariables = {|
  input?: ?string;
|};
export type deleteRoleMutationResponse = {|
  +Role: ?{|
    +destroy: boolean;
  |};
|};
*/


/*
mutation deleteRoleMutation(
  $input: ID
) {
  Role {
    destroy(_id: $input)
  }
}
*/

const batch /*: ConcreteBatch*/ = {
  "fragment": {
    "argumentDefinitions": [
      {
        "kind": "LocalArgument",
        "name": "input",
        "type": "ID",
        "defaultValue": null
      }
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "deleteRoleMutation",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": null,
        "concreteType": "RoleMutations",
        "name": "Role",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "args": [
              {
                "kind": "Variable",
                "name": "_id",
                "variableName": "input",
                "type": "ID"
              }
            ],
            "name": "destroy",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Mutation"
  },
  "id": null,
  "kind": "Batch",
  "metadata": {},
  "name": "deleteRoleMutation",
  "query": {
    "argumentDefinitions": [
      {
        "kind": "LocalArgument",
        "name": "input",
        "type": "ID",
        "defaultValue": null
      }
    ],
    "kind": "Root",
    "name": "deleteRoleMutation",
    "operation": "mutation",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": null,
        "concreteType": "RoleMutations",
        "name": "Role",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "args": [
              {
                "kind": "Variable",
                "name": "_id",
                "variableName": "input",
                "type": "ID"
              }
            ],
            "name": "destroy",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "text": "mutation deleteRoleMutation(\n  $input: ID\n) {\n  Role {\n    destroy(_id: $input)\n  }\n}\n"
};

module.exports = batch;
