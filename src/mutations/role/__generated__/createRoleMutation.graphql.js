/**
 * @flow
 * @relayHash 7d6c57976162c77564a1fbe378a4be86
 */

/* eslint-disable */

'use strict';

/*::
import type {ConcreteBatch} from 'relay-runtime';
export type createRoleMutationVariables = {|
  input?: ?{
    name: string;
    permissions: $ReadOnlyArray<?string>;
  };
|};
export type createRoleMutationResponse = {|
  +Role: ?{|
    +create: {|
      +_id: string;
      +name: string;
      +active: boolean;
    |};
  |};
|};
*/


/*
mutation createRoleMutation(
  $input: CREATE_ROLE
) {
  Role {
    create(body: $input) {
      _id
      name
      active
    }
  }
}
*/

const batch /*: ConcreteBatch*/ = {
  "fragment": {
    "argumentDefinitions": [
      {
        "kind": "LocalArgument",
        "name": "input",
        "type": "CREATE_ROLE",
        "defaultValue": null
      }
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "createRoleMutation",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": null,
        "concreteType": "RoleMutations",
        "name": "Role",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "args": [
              {
                "kind": "Variable",
                "name": "body",
                "variableName": "input",
                "type": "CREATE_ROLE"
              }
            ],
            "concreteType": "Role",
            "name": "create",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "_id",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "name",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "active",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Mutation"
  },
  "id": null,
  "kind": "Batch",
  "metadata": {},
  "name": "createRoleMutation",
  "query": {
    "argumentDefinitions": [
      {
        "kind": "LocalArgument",
        "name": "input",
        "type": "CREATE_ROLE",
        "defaultValue": null
      }
    ],
    "kind": "Root",
    "name": "createRoleMutation",
    "operation": "mutation",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": null,
        "concreteType": "RoleMutations",
        "name": "Role",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "args": [
              {
                "kind": "Variable",
                "name": "body",
                "variableName": "input",
                "type": "CREATE_ROLE"
              }
            ],
            "concreteType": "Role",
            "name": "create",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "_id",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "name",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "active",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "text": "mutation createRoleMutation(\n  $input: CREATE_ROLE\n) {\n  Role {\n    create(body: $input) {\n      _id\n      name\n      active\n    }\n  }\n}\n"
};

module.exports = batch;
