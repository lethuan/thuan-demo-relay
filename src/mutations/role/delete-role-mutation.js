// 1
import {
  commitMutation,
  graphql,
} from 'react-relay'
import { ConnectionHandler } from 'relay-runtime'
import environment from '../../configs/createRelayEnvironment'
// 2
const mutation = graphql`
mutation deleteRoleMutation($input: ID){
  Role{
    destroy(_id: $input)
  }
}
`
function sharedUpdater(store, id, deletedID) {
  const userProxy = store.get(id);
  const conn = ConnectionHandler.getConnection(
    userProxy,
    'TodoList_todos',
  );
  ConnectionHandler.deleteNode(
    conn,
    deletedID,
  );
}

// 3
export default (id, callback) => {
  // 4
  const variables = {
    input: id,
  }
  // 5
  commitMutation(
    environment,
    {
      mutation,
      variables,
      // 6
      onCompleted: () => {
        callback()
      },
      onError: err => console.error(err),
      updater: (store) => {
        // console.log('Store: ', store)
        const payload = store.getRootField('Role')
        console.log('payload: ', payload)
        const newEdge = payload.getLinkedRecord('destroy')
        console.log('newEdge: ', newEdge)
        // sharedUpdater(store, id, newEdge)
        // const record = store.get(id)
        // console.log('Id: ', id)
        // console.log('record: ', record)
      }
    },
  )
}
