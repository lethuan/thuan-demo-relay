// 1
import {
  commitMutation,
  graphql,
} from 'react-relay'
// import { ConnectionHandler } from 'relay-runtime'
import environment from '../../configs/createRelayEnvironment'

// 2
const mutation = graphql`
mutation createRoleMutation($input: CREATE_ROLE){
  Role{
    create(body: $input){
      _id
      name
      active
    }
  }
}
`

// 3
export default (name, permissions, callback) => {
  // 4
  const variables = {
    input: {
      name,
      permissions
    },
  }

  // 5
  commitMutation(
    environment,
    {
      mutation,
      variables,
      // 6
      onCompleted: () => {
        callback()
      },
      onError: err => console.error(err)
    },
  )
}
