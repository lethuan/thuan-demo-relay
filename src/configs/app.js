import moment from 'moment'

const LocalStoragePrefix = (process.env.NODE_ENV === 'production') ? 'zody-' : 'zody-dev-'
const startDate = moment().add(7, 'd').startOf('d')

export default {
  name: 'Zody Merchant',
  // endpoint: 'http://127.0.0.1:5005',
  // endpoint: 'http://125.212.219.219:3001/graphql',
  // endpoint: 'https://api.zody.vn',
  endpoint: 'http://192.168.20.88:3001/graphql',

  // Screen size
  screens: {
    'xs-max': 480,
    'sm-min': 481,
    'sm-max': 767,
    'md-min': 768,
    'md-max': 991,
    'lg-min': 992,
    'lg-max': 1199,
    'xl-min': 1200
  },

  // Local storage
  localStorage: {
    authKey: `${LocalStoragePrefix}merchant`,
    openChatRoomId: `${LocalStoragePrefix}openChatRoomId`,
    adminToken: `${LocalStoragePrefix}admin`
  },

  // Notification level
  notification: {
    success: 'success',
    error: 'error',
    warning: 'warning',
    info: 'info'
  },

  // Regex
  regex: {
    email: /\S+@\S+\.\S+/
  },

  // Format
  format: {
    date: 'DD/MM/YYYY, HH:mm',
    dateWithNoHour: 'DD/MM/YYYY',
    dateWithDayMonthOnly: 'DD/MM',
    dateWithHour: 'H',
    dateWithMinute: 'm'
  },

  // App user roles
  roles: {
    admin: 'admin',
    merchant: 'business',
    staff: 'staff',
    customercare: 'customercare'
  },

  // Components default data
  components: {
    customerTypes: {
      default: 'registered',
      list: [{
        id: 'all',
        text: 'Tất cả'
      }, {
        id: 'registered',
        text: 'Đã đăng ký'
      }, {
        id: 'unregistered',
        text: 'Chưa đăng ký'
      }]
    },
    customerLevels: {
      default: 'all',
      list: [{
        id: 'all',
        text: 'Tất cả'
      }, {
        id: 'registered',
        text: 'Đã đăng ký'
      }, {
        id: 'unregistered',
        text: 'Chưa đăng ký'
      }, {
        id: 'vip',
        text: 'V.I.P'
      }, {
        id: 'normal',
        text: 'Thường'
      }, {
        id: 'new',
        text: 'Mới'
      }, {
        id: 'lost',
        text: 'Nguy cơ mất'
      }]
    },
    campaignAutomations: {
      default: 'all',
      list: [{
        id: 'all',
        text: 'Tất cả'
      }, {
        id: 'new',
        text: 'Mới'
      }, {
        id: 'winback',
        text: 'Nguy cơ mất'
      }, {
        id: 'birthday',
        text: 'Sinh nhật'
      }]
    },
    campaignAutomationTypes: {
      default: 'new',
      list: [{
        id: 'new',
        text: 'Mới'
      }, {
        id: 'lost',
        text: 'Nguy cơ mất'
      }, {
        id: 'happy_birthday',
        text: 'Sinh nhật'
      }]
    },
    campaignNormalTypes: {
      default: 'all',
      list: [{
        id: 'all',
        text: 'Tất cả'
      }, {
        id: 'normal',
        text: 'Bình thường'
      }, {
        id: 'vip',
        text: 'V.I.P'
      }, {
        id: 'lost',
        text: 'Nguy cơ mất'
      }, {
        id: 'new',
        text: 'Mới'
      }]
    },
    campaignNormalListTypes: {
      default: 'not-filter',
      list: [{
        id: 'not-filter',
        text: '--'
      }, {
        id: 'all',
        text: 'Tất cả'
      }, {
        id: 'normal',
        text: 'Bình thường'
      }, {
        id: 'vip',
        text: 'V.I.P'
      }, {
        id: 'lost',
        text: 'Nguy cơ mất'
      }, {
        id: 'new',
        text: 'Mới'
      }]
    },
    campaignCustomerLevels: {
      default: 'all',
      list: [{
        id: 'all',
        text: 'Tất cả'
      }, {
        id: 'normal',
        text: 'Bình thường'
      }, {
        id: 'vip',
        text: 'V.I.P'
      }, {
        id: 'win_back',
        text: 'Nguy cơ mất'
      }, {
        id: 'new',
        text: 'Mới'
      }]
    },
    campaignStatus: {
      default: 'all',
      list: [{
        id: 'all',
        text: 'Tất cả'
      }, {
        id: 'closed',
        text: 'Hủy'
      }, {
        id: 'completed',
        text: 'Hoàn thành'
      }, {
        id: 'pending',
        text: 'Đang chờ duyệt'
      }, {
        id: 'published',
        text: 'Đang chạy'
      }, {
        id: 'rejected',
        text: 'Bị từ chối'
      }]
    },
    campaignAppStatus: {
      default: 'all',
      list: [{
        id: 'all',
        text: 'Tất cả'
      }, {
        id: 'registered',
        text: 'Đã cài Zody'
      }, {
        id: 'unregistered',
        text: 'Chưa cài Zody'
      }]
    },
    campaignSendingBefore: {
      defaultOne: '7',
      defaultTwo: '14',
      defaultThree: '30',
      listOne: [{
        id: '7',
        text: '7 ngày',
        expireAfterDays: 7
      }, {
        id: '14',
        text: '2 tuần',
        expireAfterDays: 14
      }, {
        id: '28',
        text: '1 tháng',
        expireAfterDays: 30
      }],
      listTwo: [{
        id: '14',
        text: '2 tuần',
        expireAfterDays: 14
      }, {
        id: '30',
        text: '1 tháng',
        expireAfterDays: 15
      }, {
        id: '45',
        text: '1.5 tháng',
        expireAfterDays: 30
      }],
      listThree: [{
        id: '30',
        text: '1 tháng',
        expireAfterDays: 30
      }, {
        id: '60',
        text: '2 tháng',
        expireAfterDays: 30
      }, {
        id: '90',
        text: '3 tháng',
        expireAfterDays: 30
      }],
    },
    rangePicker: {
      // start: moment().subtract(6, 'd').startOf('d').toISOString(), // 7days
      start: moment().subtract(30, 'd').startOf('d').toISOString(),
      end: moment().endOf('d').toISOString()
    },
    feedback: {
      default: 'all',
      list: [{
        id: 'all',
        text: 'Tất cả'
      }, {
        id: 'has-feedback',
        text: 'Có góp ý'
      }, {
        id: 'no-feedback',
        text: 'Không có góp ý'
      }],
      stars: [{
        id: 'five',
        text: '5',
        color: '#FF6701',
        percent: 0,
        value: 0
      }, {
        id: 'four',
        text: '4',
        color: '#FF6701',
        percent: 0,
        value: 0
      }, {
        id: 'three',
        text: '3',
        color: '#FF862F',
        percent: 0,
        value: 0
      }, {
        id: 'two',
        text: '2',
        color: '#FFB380',
        percent: 0,
        value: 0
      }, {
        id: 'one',
        text: '1',
        color: '#FFE1CD',
        percent: 0,
        value: 0
      }]
    },
    reward: {
      statuses: {
        all: 'all',
        used: 'used',
        notUsedYet: 'notUsedYet'
      }
    },
    chat: {
      default: 'all',
      types: [{
        id: 'all',
        text: 'Tất cả'
      }, {
        id: 'unread',
        text: 'Chưa đọc'
      }]
    }
  },

  // App display text
  displayText: {
    activities: [{
      id: 'bill',
      text: 'Tích luỹ hoá đơn',
      icon: 'iconBill'
    }, {
      id: 'checkin',
      text: 'Check-in',
      icon: 'iconCheckin'
    }],
    cities: [{
      id: 'da-nang',
      text: 'Đà Nẵng'
    }, {
      id: 'ho-chi-minh',
      text: 'TPHCM'
    }],
    genders: [{
      id: 'male',
      text: 'Nam'
    }, {
      id: 'female',
      text: 'Nữ'
    }]
  },

  // App colors
  colors: {
    app: '#F60',
    coin: '#E4C600',
    revenue: '#87D068',
    transaction: '#F60',
    male: '#F60',
    female: '#909090',
    time: '#F60',
    graphColor: '#5a5a5a',

    // Common
    white: '#fff',
    black: '#000'
  },

  // Go chat types
  goChatTypes: {
    customerInfo: 'customerInfo',
    reply: 'reply'
  },

  // Campaign normal
  campaignNormal: {
    names: [
      'Tất cả',
      'Mới',
      'Bình thường',
      'V.I.P',
      'Nguy cơ'
    ],
    campaignCity: {
      default: 'all',
      list: [{
        id: 'all',
        text: 'Tất cả'
      }, {
        id: 'da-nang',
        text: 'Đà Nẵng'
      }, {
        id: 'ho-chi-minh',
        text: 'TPHCM'
      }]
    },
    campaignGender: {
      default: 'all',
      list: [{
        id: 'all',
        text: 'Tất cả'
      }, {
        id: 'male',
        text: 'Nam'
      }, {
        id: 'female',
        text: 'Nữ'
      }]
    },
    rangePickerCampaign: {
      start: moment().add(7, 'd').startOf('d').toISOString(),
      end: moment(startDate).add(1, 'M').endOf('d').toISOString()
    }
  },
  campaignAutomation: {
    type: {
      default: 'new',
      list: [{
        id: 'new',
        text: 'Mới'
      }, {
        id: 'winback',
        text: 'Nguy cơ mất'
      }, {
        id: 'birthday',
        text: 'Sinh nhật'
      }]
    },
    winBack: [{
      default: '7',
      list: [{
        id: '7',
        text: '7 ngày',
        expireAfterDays: 7
      }, {
        id: '14',
        text: '2 tuần',
        expireAfterDays: 14
      }, {
        id: '28',
        text: '1 tháng',
        expireAfterDays: 30
      }, {
        default: '14',
        list: [{
          id: '14',
          text: '2 tuần',
          expireAfterDays: 14
        }, {
          id: '30',
          text: '1 tháng',
          expireAfterDays: 15
        }, {
          id: '45',
          text: '1.5 tháng',
          expireAfterDays: 30
        }]
      }, {
        default: '30',
        list: [{
          id: '30',
          text: '1 tháng',
          expireAfterDays: 30
        }, {
          id: '60',
          text: '2 tháng',
          expireAfterDays: 30
        }, {
          id: '90',
          text: '3 tháng',
          expireAfterDays: 30
        }]
      }]
    }]
  }
}
