import {
  Environment,
  Network,
  RecordSource,
  Store,
} from 'relay-runtime'

function fetchQuery(operation, variables, cacheConfig, uploadables) {
  const request = {
    method: 'POST',
    headers: {
      // 'Accept': 'application/json',
      // 'Content-Type': 'multipart/form-data'
    }
  }
  if (uploadables) {
    if (!window.FormData) {
      throw new Error('Uploading files without `FormData` not supported.')
    }
    const formData = new FormData()
    formData.append('operations', JSON.stringify({ query: operation.text }))
    formData.append('map', JSON.stringify({ file: ['variables.file'] }))
    Object.keys(uploadables).forEach((key) => {
      if (Object.prototype.hasOwnProperty.call(uploadables, key)) {
        formData.append(key, uploadables[key])
      }
    })
    request.body = formData
  } else {
    request.headers['Content-Type'] = 'application/json'
    request.body = JSON.stringify({
      query: operation.text,
      variables
    })
  }


  return fetch('https://dev-doc-api.zody.vn/graphql', request).then((response) => {
    if (response.status === 200) {
      return response.json()
    }
    return response.json()
  }).catch((error) => {
    console.log(error)
  })
}

const source = new RecordSource();
const store = new Store(source);

// singleton Environment
export default new Environment({
  // network: networkLayer,
  // handlerProvider,
  network: Network.create(fetchQuery),
  store,
});
