export default (permissions, CRUD) => {
  const isCRUD = {
    isCreate: true,
    isRead: true,
    isUpdate: true,
    isDelete: true
  }

  for (const value of permissions) {
    if (value === CRUD[0]) {
      if (value) {
        isCRUD.isCreate = false
      }
    } else if (value === CRUD[1]) {
      if (value) {
        isCRUD.isRead = false
      }
    } else if (value === CRUD[2]) {
      if (value) {
        isCRUD.isUpdate = false
      }
    } else if (value === CRUD[3]) {
      if (value) {
        isCRUD.isDelete = false
      }
    }
  }
  return isCRUD
}
